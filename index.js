// console.log("Hello World");

function printInfo() {
	let nickname = prompt("Enter your nickname: ");
	console.log("Hi, " + nickname);
}

// printInfo(); //invoke

					//parameter
function printName(name) {
	console.log("My name is " + name);
}


printName("Rico"); //Function with an arguement with string value "Rico" that was passed

printName("Juana");

printName("Cena");

// Now we have a reusable runction / reusable task but could have different output based on what value to process with the help of funciton parameters and arguements.

// [SECTION] Parameters and Arguements


// Parameter
	// "firstName" is called a parameter
	// A "parameter" acts as named variable/container that exists only inside a function.
	// It is used to store information that is provided to a function when it is called/invoked.

// Arguement
	// "Juana", "John", and "Cena" are the information/data provided/passed directly into the funciton is called an argument.
	// Values passed when invoking a function are called arguments.
	// These arguments are then stored as the parameter within the function.


let sampleVariable = "Inday";

printName(sampleVariable);
// Variables can also be passed as an argument

//---------------------------

function checkDivisibility(num, divisor) {
	let remainder = num % divisor;
	console.log(`The remainder of ${num} divided by ${divisor} is: ` + remainder);
	let isDivisibleBy8 = remainder === 0;

	console.log(`Is ${num} divisible by ${divisor}?`);
	console.log(isDivisibleBy8);
}

checkDivisibility(64, 8);
checkDivisibility(28, 8);

// [SECTION] Function as argument
	// Function parameters can also accept functions as arguments
	// Some complex functions suses other functions to perform more complicated results.


	function argumentFunction() {
		console.log("This function was passed as an argument before the message was printed.");
	}

	function invokeFunction(argumentFunction) {
		argumentFunction();
	}

	invokeFunction(argumentFunction);

	console.log(argumentFunction);

// -----------------------------------------------

// [SECTION] Using Multiple Parameters

	function createFullName(firstName, middleName, lastName) {
		console.log(`My full name is ${firstName} ${middleName} ${lastName}`);

	}

	createFullName("Enrico Emil", "Yu", "Dela Rosa");


	// Using variables as an argument
	let firstName = "John";
	let middleName = "Doe";
	let lastName = "Smith";

	createFullName(firstName, middleName, lastName);

	function getDifferenceOfAMinusB(numA,numB) {
		console.log(`Difference of ${numA} and ${numB}: ` + (numA - numB));
	}
	

	getDifferenceOfAMinusB(8,4);


// [SECTION] Return statement


	// The "return" statement allows us to ouptut a value from a function to be passed to the line/block of code that is invoked/called
	function returnFullName(firstName, middleName, lastName) {
		 

		// We could also create a variable inside the funciton to contain the result and return the variable instead.

		let fullName = `${firstName} ${middleName} ${lastName}`;

		return fullName;

		// lines of code after the return statement will no longer be read
 	}

 	let completeName = returnFullName("Paul", "Smith", "Jordan");
 	console.log(completeName);

 	console.log(`I am ${completeName}`);

 	function printPlayerInfo(userName, level, job) {
 		console.log("Username: " + userName);
 		console.log("Level: " + level);
 		console.log("Job: " + job);
 	}

 	let user1 = printPlayerInfo("boxzmapagmahal", "Senior", "Programmer");
 	console.log(user1);







